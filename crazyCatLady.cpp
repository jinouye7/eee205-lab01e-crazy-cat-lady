///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ summation 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author  Jared Inouye <jinouye7@hawaii.edu>
/// @date    10_jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

int main( int argc, char* argv[] ) {
	std::cout << "Crazy Cat Lady!" << std::endl ;
   //commented out printf changed to cout
   //printf("Oooooh! %s, you're so cute!\n", argv[1]);
   std::cout << "Oooooh! " << argv[1] <<", you're so cute!" << "\n";
   return 0;
}
